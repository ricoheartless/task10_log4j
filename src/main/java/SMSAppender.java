import com.twilio.example.Example;
import java.io.Serializable;
import java.security.SecureRandom;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name="SMS",category = "Core", elementType = "appender", printObject = true)
public class SMSAppender extends AbstractAppender {
  protected SMSAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions){
    super(name,filter,layout,ignoreExceptions);
  }

  public void append(LogEvent logEvent) {
      try {
        ExampleSMS.send(new String(getLayout().toByteArray(logEvent)));
      }
      catch (Exception ex){}
  }
  @PluginFactory
  public static SMSAppender createAppender(
      @PluginAttribute("name") String name,
      @PluginElement("Layout") Layout <? extends Serializable> layout,
      @PluginElement("Filter") final Filter filter,
      @PluginAttribute("otherAttribute") String otherAttribute){
    if(name == null){
      LOGGER.error("No name provided to customApeenderImpl");
      return null;
    }
    if(layout == null){
      layout = PatternLayout.createDefaultLayout();
    }
    return new SMSAppender(name,filter,layout,true);
  }
}
