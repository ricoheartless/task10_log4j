import logger.MyLogger;

public class ArithmeticTest {
  public static void main(String[] args) {
   ArithmeticOperation operation = new ArithmeticOperation(2,0);
   MyLogger logger = new MyLogger();
   try {
     operation.divide();
   }
   catch (Exception e){
     logger.performTask();
   }
  }
}
