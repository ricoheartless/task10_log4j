import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
  public static final String ACCOUNT_SID = "ACd36e7fd7049368dff13629cf40945786";
  public static final String AUTH_TOKEN = "73a2ed6dda50730cd4ec9246b805efc8";

  public static void send(String text){
    Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
    Message message = Message.creator(new PhoneNumber("+380934414717"), new PhoneNumber("+12172866463"), text).create();
  }
}
